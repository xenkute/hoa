const loginPage = require('../../pages/login');
// const leftSidebar = require('../../pages/leftSidebar');
const newPatientPage = require('../../pages/newpatient');

describe('Patients', () => {

  it('login', () => {
    loginPage.open()
             .login('apple', 'testtest123');
  });

  it('create new patient', () => {
    newPatientPage.open();
    newPatientPage.photo.setValue(browser.uploadFile('images/autumn.jpg'));
    newPatientPage.firstname.setValue('Test 1');
    newPatientPage.middlename.setValue('test 1 middle');
    newPatientPage.lastname.setValue('last 1');
    newPatientPage.maidenFirst.setValue('test maiden');
    newPatientPage.maidenLast.setValue('test maiden last');
    newPatientPage.dob.setValue('07/01/2008');
    newPatientPage.sex.selectByVisibleText('Female');
    newPatientPage.ssn.setValue('434234354');
    newPatientPage.selectPreferedLanguage("English");
    newPatientPage.ethnicity.selectByVisibleText("Hispanic or Latino");
    newPatientPage.selectRace("Asian");
    newPatientPage.hippa.selectByVisibleText('Phone');
    newPatientPage.maritalStatus.selectByVisibleText('Married');
    newPatientPage.spouse.setValue('test spouse');
    newPatientPage.occupationStatus.selectByVisibleText('Employed');
    newPatientPage.employer.setValue('company test');
    newPatientPage.vaccinePublicityMethod.selectByVisibleText('Reminder only - any method');
    newPatientPage.vaccineDataProtection.click();
    newPatientPage.vaccinePublicityDate.setValue('07/01/2020');
    newPatientPage.vaccineDataProtectionDate.setValue('07/31/2020');

    newPatientPage.inputAddressInfo('123 test street', 'brooklyn', 'HI', '23452');
    newPatientPage.inputAddressInfo('345 test', 'texas', 'CA', '56465432', { index: 2 });
    newPatientPage.inputPhoneInfo('Mobile', '3245436546');
    newPatientPage.inputPhoneInfo('Home', '4324231432', { index: 2 });
    newPatientPage.inputEmailInfo('Work', 'a@gmail.com');

    newPatientPage.emergencyFirstName.setValue('test emergency');
    newPatientPage.emergencyLastName.setValue('last emer');
    newPatientPage.emergencyRelationship.selectByVisibleText('Spouse');
    newPatientPage.emergencyStreet.setValue('123 street');
    newPatientPage.emergencyCity.setValue('hov');
    newPatientPage.emergencyState.selectByVisibleText('AR');
    newPatientPage.emergencyZip.setValue('34435');
    newPatientPage.emergencyHomePhone.setValue('4321234234');
    newPatientPage.emergencyCellPhone.setValue('3421243243');
    newPatientPage.emergencyWorkPhone.setValue('343214231432123');
    newPatientPage.emergencyEmail.setValue('b@gmail.com');

    newPatientPage.guardianFirstName.setValue('agent test');
    newPatientPage.guardianLastName.setValue('agent last');
    newPatientPage.guardianRelationship.selectByVisibleText('Friend');
    newPatientPage.guardianStreet.setValue('123 more street');
    newPatientPage.guardianCity.setValue('texas');
    newPatientPage.guardianState.selectByVisibleText('ID');
    newPatientPage.guardianZip.setValue('54354');
    newPatientPage.guardianHomePhone.setValue('5436576576');
    newPatientPage.guardianCellPhone.setValue('5432543255');
    newPatientPage.guardianWorkPhone.setValue('1433454354');
    newPatientPage.guardianNote.setValue('this is the note for test');
  });

})
