# README #

Please follow these steps below to run this project.

### What is this repository for? ###

* Money

### How do I get set up? ###

* This project use nodeJS version 12.18.2 and npm version 6.14.5. Please go to [here](https://nodejs.org/en/download/) to install and use these commands to check the version: node -v && npm -v
* Check out this project and make it a current working directory by command: git clone https://xenkute@bitbucket.org/xenkute/hoa.git && cd hoa
* Install all dependencies of this project by command: npm install
* Run the automation task and view report by command: npm run test && npm run report

### Contact? ###

* vhtien1986@gmail.com
* Skype: vhtien1986
