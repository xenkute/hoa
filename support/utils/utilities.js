const fs = require('fs');

const takeScreenshot = (name, failure=false) => {
  const path = './report/screenshot/';
  fs.access(path, fs.F_OK, err => {
    if (err) {
      console.log("File path doesn't exists ... Let's create!");
      fs.mkdir(path, { recursive: true }, err => {});
    }
  });

  if (failure) {
    name = name + '_failed';
  }

  name = name.replace(/ /g, '_') + '.png';
  browser.pause(500);
  browser.saveScreenshot(path + name);
  fs.readFile(`${path}${name}`, (err, data) => allure.addAttachment(name, data, 'image/png'));
};

module.exports = {
  takeScreenshot
}
