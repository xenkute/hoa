const Page = require('./page');

class Patient extends Page {

  get addNewPatient() = { return $('.btn-primary') }

}

module.exports = new Patient();
