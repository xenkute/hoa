const Page = require('./page');

const URL = 'account/login';

class Login extends Page {

  get username() { return $('#UserNameReal') }
  get password() { return $('#PasswordReal') }
  get location() { return $('#LocationId') }
  get submit() { return $('.btn-primary') }

  open() {
    super.open(URL);
    return this;
  }

  login(user, pass, loc=undefined) {
    this.username.setValue(user);
    this.password.setValue(pass);

    if (loc !== undefined) {
      this.location.selectByVisibleText(loc);
    }

    this.submit.click();

    browser.waitUntil(
      () => !browser.getUrl().includes(URL)
    );
  }
}

module.exports = new Login();
