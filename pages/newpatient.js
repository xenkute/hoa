const Page = require('./page');

const URL = 'patient/create';

class NewPatient extends Page {

  get photo() { return $('#PatientImgFile') }
  get firstname() { return $('#FirstName') }
  get middlename() { return $('#MiddleName') }
  get lastname() { return $('#LastName') }
  get maidenFirst() { return $('#MaidenFirst') }
  get maidenLast() { return $('#MaidenLast') }
  get dob() { return $('#DOB') }
  get sex() { return $('#Sex') }
  get ssn() { return $('#SSN') }
  get active() { return $('#Active') }
  get preferedLanguage() { return $('#select2-PreferredLanguage-container') }
  get ethnicity() { return $('#Ethnicity') }
  get race() { return $('.select2-search__field') }
  get hippa() { return $('#HIPAAReminderPreference') }
  get maritalStatus() { return $('#MaritalStatus') }
  get spouse() { return $('#Spouse') }
  get occupationStatus() { return $('#OccupationStatus') }
  get employer() { return $('#Employer') }
  get vaccinePublicityMethod() { return $('#VaccinePublicityMethod') }
  get vaccinePublicityDate() { return $('#VaccinePublicityDate') }
  get vaccineDataProtection() { return $('//*[@id="VaccineDataProtection"]/..') }
  get vaccineDataProtectionDate() { return $('#VaccineDataProtectionDate') }

  // *************** contact info ***************

  get addressAddNew() { return $('#DetailsPatientAddressListAddBtn') }
  get phoneAddNew() { return $('#DetailsPatientPhoneListAddBtn') }
  get emailAddNew() { return $('#DetailsPatientEmailListAddBtn') }

  // *************** emergency contact ***************

  get emergencyFirstName() { return $('#EmergencyContact_FirstName') }
  get emergencyLastName() { return $('#EmergencyContact_LastName') }
  get emergencyRelationship() { return $('#EmergencyContact_Relationship') }

  get emergencyStreet() { return $('#EmergencyContact_Address') }
  get emergencyCity() { return $('#EmergencyContact_City') }
  get emergencyState() { return $('#EmergencyContact_State') }
  get emergencyZip() { return $('#EmergencyContact_Zip') }

  get emergencyHomePhone() { return $('#EmergencyContact_HomePhone') }
  get emergencyCellPhone() { return $('#EmergencyContact_CellularPhone') }
  get emergencyWorkPhone() { return $('#EmergencyContact_WorkPhone') }

  get emergencyEmail() { return $('#EmergencyContact_Email') }

  // *************** guardian contact ***************

  get guardianSameAsEmergencyContact() { return $('#GuardianSameAsEmergencyContact') }

  get guardianFirstName() { return $('#GuardianContact_FirstName') }
  get guardianLastName() { return $('#GuardianContact_LastName') }
  get guardianRelationship() { return $('#GuardianContact_Relationship') }

  get guardianStreet() { return $('#GuardianContact_Address') }
  get guardianCity() { return $('#GuardianContact_City') }
  get guardianState() { return $('#GuardianContact_State') }
  get guardianZip() { return $('#GuardianContact_Zip') }

  get guardianHomePhone() { return $('#GuardianContact_HomePhone') }
  get guardianCellPhone() { return $('#GuardianContact_CellularPhone') }
  get guardianWorkPhone() { return $('#GuardianContact_WorkPhone') }

  get guardianNote() { return $('#GuardianContact_Note') }

  get saveButton() { return $('.btn-primary') }
  get cancelButton() { return $('.imps-edit-cancel-button') }

  open() {
    super.open(URL);
    return this;
  }

  selectPreferedLanguage(lang) {
    this.preferedLanguage.click();
    $('.select2-search--dropdown .select2-search__field').setValue(lang);
    browser.keys("\uE007");
  }

  selectRace(race) {
    this.race.click();
    $(`//*[@id='select2-patientRaceSelect-results']/li[text()='${race}']`).click();
  }

  inputAddressInfo(street, city, state, zip, { primary=false, index=1 } = {}) {
    if (index > 1) {
      this.addressAddNew.click();
    }
    $(`#PatientAddresses_${index}__Street`).setValue(street);
    $(`#PatientAddresses_${index}__City`).setValue(city);
    $(`#PatientAddresses_${index}__State`).selectByVisibleText(state);
    $(`#PatientAddresses_${index}__Zip`).setValue(zip);

    if (primary && index > 1) {
      $(`//*[@id="PatientAddresses_${index}__State"]/../../..//a`).click();
    }
  }

  inputPhoneInfo(type, number, { primary=false, index=1 } = {}) {
    if (index > 1) {
      this.phoneAddNew.click();
    }
    $(`#PatientPhones_${index}__ContactTypeId`).selectByVisibleText(type);
    $(`#PatientPhones_${index}__ContactText`).setValue(number);

    if (primary && index > 1) {
      $(`//*[@id="PatientPhones_${index}__ContactTypeId"]/../../..//a`).click();
    }
  }

  inputEmailInfo(type, email, { primary=false, index=1 } = {}) {
    if (index > 1) {
      this.emailAddNew.click();
    }
    $(`#PatientEmails_${index}__ContactTypeId`).selectByVisibleText(type);
    $(`#PatientEmails_${index}__ContactText`).setValue(email);

    if (primary && index > 1) {
      $(`//*[@id="PatientEmails_${index}__ContactTypeId"]/../../..//a`).click();
    }
  }
}

module.exports = new NewPatient();
