const Page = require('./page');

class LeftSidebar extends Page {

  get patient() { return $('a[href="/Patient"]') }

}

module.exports = new LeftSidebar();
